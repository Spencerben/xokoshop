This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Getting XOKOSHOP up and running

The following steps will help you setup  the app on your local computer:

* `1` clone this repository into your computer.
* `2` cd into the root folder of the APP.
* `3` run npm install.
* `4` once complete, run npm start the app will load on http://localhost:3000/.

## User journey from start to finish

The following steps will help you be able to test the various steps as defined in the test:

NB: I have connected to the MOCK server API as I was unable to gain acces to the production server

* `1` Login with any combination of email and password;Due to time limit, I did not implement validation yet.
* `2` You will be redirected to the outlets.
* `3` Click on an outlet to view the categories.
* `4` Click on a category to view the products.
NB: Only the curtains category has products 
* `5` Click on a product to proceed to the pop up..
* `6` Update the quantity, and select payment method.
* `7` Click buy, an alert box will show to confirm your purchase.
