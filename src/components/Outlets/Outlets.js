import React, { Component } from 'react';
import './Outlets.css';
import Outlet from './Outlet/Outlet';
import { GetOutletsData } from '../../models/getOutletsApi';

class Outlets extends Component {
  constructor(props){
      super(props);

      this.state = { outlets: [] };
      this.updateTitle = this.updateTitle.bind(this);
      this.getOutlets = this.getOutlets.bind(this);
    }
  
    updateTitle(e) {
      this.props.onTitleChange(e.target.value);
    }
    
    async getOutlets() {
      GetOutletsData().then((outlets) => {
        this.setState({ outlets });
      });
    }
  
    componentDidMount() {
      this.getOutlets();
    }
  
  render() {
    const { outlets }  = this.state
    return (
      <div>
        {
          outlets.map((outlet,index) => {
            return <Outlet outletDetails = {outlet} key = {index} />
          })
        }
      </div>
    );
  }
}

export default Outlets;
