import React, { Component } from 'react';
import './Outlet.css';
class Outlet extends Component {

    constructor(props){
        super(props);

        this.state = { 
            outletDetails : this.props.outletDetails,
            key: '',
            id: null
        }
        this.getOutlets = this.getOutlets.bind(this);
        this.showCategories = this.showCategories.bind(this);
    }
    getOutlets(){
        this.setState({
            outletDetails: this.props.outletDetails,
            key: this.props.key
        });
    }
    
    showCategories(e){
        e.preventDefault();
        return false;
    }
    componentDidMount() {
        
      }


  render() {
      const outlet = this.state.outletDetails;
    return (
        <div className=" col-md-4">
            <a href={"/categories/"+ outlet.id}>
                <div className="paper" id={outlet.id}>
                    <div className="content content__narrow">
                        <div className="article intercom-force-break">
                            <div className="article__meta" dir="ltr">
                                <h3>{outlet.name}</h3>
                                <ul>
                                    <li><b>name :</b> {outlet.name}</li>
                                    <li><b>address :</b> {outlet.address}</li>
                                    <li><b>phone :</b> {outlet.phone}</li>
                                    <li><b>email :</b> {outlet.email}</li>
                                </ul>
                                <br />
                               <button className="btn btn-primary btn-block br3 btn-lg" type="button" >View Categories</button>
            
                                
                            </div>
                        </div>
                    </div>
                </div>
            </a>  
        </div>

    );
  }
}

export default Outlet;
