import React, { Component } from 'react';
import './Login.css';

class Login extends Component {

  API_URL = "https://private-5525a9-bambapospub.apiary-mock.com/registerApp";
  
  constructor(props) {
    super(props);
    
    this.state = {
      email: "",
      password: "" ,
      title: "Login",
      consumerKey : '',
      consumerSecret : '',
      categId:''
    }

    this.onSubmit = this.onSubmit.bind(this);
    this.getLoginDetails = this.getLoginDetails.bind(this);
    this.getLoginToken = this.getLoginToken.bind(this);

  }
  getEmail(){
    return this.state.email;
  }
  getPassword(){
    return this.state.password;
  }
  getLoginDetails() {
    return  this.state;
  }

  getLoginToken() {

    fetch(this.API_URL, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "email": this.getEmail(),
        "password": this.getPassword()
      })
    }).then(res => res.json())
    .catch((error) => {

      console.error('Error:', error);
      
      this.setState({
        consumerKey : '',
        consumerSecret : ''
      });
    })
    .then(data => {
      this.setState({
        consumerKey : data.response.ConsumerKey,
        consumerSecret : data.response.ConsumerSecret
      });
       const token = {
         consumerKey : data.response.ConsumerKey,
        consumerSecret : data.response.ConsumerSecret
      }
      localStorage.setItem('userToken', JSON.stringify(token));
       this.props.history.push("/outlets");
      
    }
  );
    
  }

  
  onSubmit(e) {
    e.preventDefault();

    this.setState({
      email: this.email.value,
      password: this.password.value
    });
    
    this.getLoginToken();
  }

  render() {
    return (
      <div id="fh5co-contact" className="">
        <div className="container m-t-30 m-b-30">
          <div className="row">
            <div className="col-md-12 text-center fh5co-heading">
              <h3 className="fcorange"><b>{this.state.title}</b></h3>
            </div>
          </div>
          <div className="row">
            <div className="col-md-8 col-md-offset-2">
              <div className="loginForm">
                <form className="contact-form text-caps" onSubmit = {this.onSubmit}>
                  <div className="form-group">
                    <label htmlFor="email" className="control-label">Email*</label>
                    <input  
                      ref = {email => this.email = email} 
                      className="form-control" 
                      name="email" 
                      type="text" 
                    />	
                  </div>
                  <div className="form-group">
                    <label htmlFor="password" className="control-label">Password *</label>
                    <input  
                        ref = {password => this.password = password} 
                        className="form-control" 
                        name="password" 
                        type="password" 
                        />	
                  </div>
                  <div className="form-group">
                    <input value="Submit" className="btn btn-primary btn-block br3 btn-lg" type="submit" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Login.PropTypes = this.propTypes;
export default Login;
