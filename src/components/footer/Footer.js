import React, { Component } from 'react';
import logo from '../../logo.png';
class Footer extends Component {
    

    render() {
        return (
			<footer id="fh5co-footer" className="p-md-30" role="contentinfo">
			<div className="container-fluid">
				<div className="row row-pb-md">
					<div className="col-md-12">
						<div className="col-md-6">
							<h1>
								<img src={logo} className="footerlogo"  alt="logo"/><br />
								
							</h1>
						</div>
						<div className="col-md-6 text-right p-20">
							
							<p className="m-t-20 fs08">XOKOshop - Benson N. K</p>
						</div>
					</div>
					
				</div>
	
				
			</div>
		</footer>
        )

    }
}
export default Footer;