import React, { Component } from 'react';

import logo from '../../logo.png';
class Navigation extends Component {
    

    render() {
        return (
            <div className="fh5co-nav" role="navigation">
		<div className="top-menu">
			<div className="container">
				<div className="row">
					<div className="col-xs-2">
						<div id="fh5co-logo"><a href="/"><img src={logo} alt="logo" /></a></div>
					</div>
					<div className="col-xs-10 text-right menu-1">
						<ul>
							<li><a href="/outlets">Outlets</a></li>
							<li className="btn-cta"><a href="/login"><span>Login</span></a></li>
						</ul>
					</div>
				</div>
				
			</div>
		</div>
	</div>
        )

    }
}
export default Navigation;