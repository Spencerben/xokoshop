import React, { Component } from 'react';
import './MakeASale.css';
import Modal from 'react-modal';
class MakeASale extends Component {
  customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)'
    }
  };
  constructor(props){
    super(props);

    this.state = { 
      product : this.props.product,
      categoryId:this.props.categoryId
    }
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

  }
  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  render() {

    const product = this.state.product;
    return (
      <div>
        <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={this.customStyles}
            contentLabel="Example Modal"
        >

        <h2 ref={subtitle => this.subtitle = subtitle}>Hello</h2>
        <button onClick={this.closeModal}>close</button>
        
        <div id="fh5co-contact" className="">
        <div className="modal-container m-t-30 m-b-30">
          <div className="row">
            <div className="col-md-12 text-center fh5co-heading">
              <h3 className="fcorange"><b>{product.name}</b></h3>
            </div>
          </div>
          <div className="row">
            <div className="col-md-8 col-md-offset-2">
              <div className="loginForm">
                <form className="contact-form text-caps" onSubmit = {this.onSubmit}>
                  <div className="form-group">
                    <label htmlFor="Quantity" className="control-label">Quantity*</label>
                    <input  
                      ref = {quantity => this.quantity = quantity} 
                      className="form-control" 
                      name="quantity" 
                      type="text" 
                    />	
                  </div>
                  <div className="form-group">
                    <label htmlFor="Discount" className="control-label">Discount *</label>
                    <input  
                        ref = {discount => this.discount = discount} 
                        className="form-control" 
                        name="discount" 
                        type="discount" 
                        disabled = "disabled"
                        />	
                  </div>
                  <div className="form-group">
                    <label htmlFor="Discount" className="control-label">Discount *</label>
                    <select name="payment_option">
                        <option name="cash">CASH</option>
                        <option name="cash">MCASH</option>
                        <option name="cash">CARD</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <input value="Submit" className="btn btn-primary btn-block br3 btn-lg" type="submit" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
        </Modal>
    </div>

    );
  }
}

export default MakeASale;
