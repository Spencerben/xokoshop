import React, { Component } from 'react';
import './Categories.css';
import Category from './Category/Category';
import { GetCategoriesData } from '../../models/getCategoriesApi';

class Categories extends Component {
  constructor(props){
      super(props);

      this.state = { categories: [] };
      this.updateTitle = this.updateTitle.bind(this);
      this.getCategories = this.getCategories.bind(this);
    }
  
    updateTitle(e) {
      this.props.onTitleChange(e.target.value);
    }
  
    async getCategories() {
      GetCategoriesData().then((categories) => {
        this.setState({ categories });
      });
      
    }
  
    componentDidMount() {
      this.getCategories();
    }
  
  render() {
    const { categories }  = this.state
    return (
      <div>
        {
          categories.map((category,index) => {
            return <Category categoryDetails = {category} key = {index} />
          })
        }
      </div>
    );
  }
}

export default Categories;
