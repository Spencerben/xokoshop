import React, { Component } from 'react';
import './Category.css';
class Category extends Component {

    constructor(props){
        super(props);

        this.state = { 
            categoryDetails : this.props.categoryDetails,
            key: ''
        }
        this.getCategories = this.getCategories.bind(this);
    }
    getCategories(){
        this.setState({
            categoryDetails: this.props.categoryDetails,
            key: this.props.key
        });
    }
    


  render() {
      const category = this.state.categoryDetails;
    return (
        <div className=" col-md-12 categories">
            <a href={"/category/" +category.id + "/Products/"}>
                <div className="paper" id={category.id} onClick={this.showProducts}>
                    <div className="content content__narrow">
                        <div className="article intercom-force-break">
                            <div className="article__meta" dir="ltr">
                                <h3>{category.name}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

    );
  }
}

export default Category;
