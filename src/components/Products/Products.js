import React, { Component } from 'react';
import './Products.css';
import Product from './Product/Product';
import { GetProductData } from '../../models/getProductsApi';

class Products extends Component {
  constructor(props){
      super(props);

      this.state = { 
        products: [] ,
        categoryProducts: [],
        categoryId: 1 
      };
      this.updateTitle = this.updateTitle.bind(this);
      this.getProducts = this.getProducts.bind(this);
      this.filterProducts = this.filterProducts.bind(this);
    }
  
    updateTitle(e) {
      this.props.onTitleChange(e.target.value);
    }
  
    async getProducts(categoryId ="") {

      GetProductData().then((products) => {

        if(categoryId ===""){
          GetProductData().then((products) => {
          this.setState({ products });

          });
        } else {

          this.filterProducts(products, categoryId);
          
        }
      });
    }

    filterProducts(allProducts, categoryId){
      let filteredProducts = [];

      allProducts.map((value,key,arr) => {
        if(allProducts[key].category === categoryId){ 
         return filteredProducts.push(arr[key]);
        } else {
          filteredProducts = [];
         return filteredProducts ;
        }
      });
      this.setState({
        products: filteredProducts,
        categoryId: categoryId
      });
    }
  
    componentDidMount() {
      let { id } = this.props.match.params;

      this.getProducts(id);
    }
  
  render() {
    const { products }  = this.state
    
    return (
      <div>
        {
          (products.length<1 || products === undefined) ?
          "There are no products available for that category" :
          (products.map((product,index) => {
            return <Product categoryId = {this.state.categoryId} productDetails = {product} key = {product.id} />
          }) )
        
        }
      </div>
    );
  }
}

export default Products;
