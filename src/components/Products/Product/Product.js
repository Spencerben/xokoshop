import React, { Component } from 'react';
import Modal from 'react-modal';
import './Product.css';
import { MakeASaleData } from '../../../models/makeASaleApi';
class Product extends Component {

     customStyles = {
        content : {
          top                   : '50%',
          left                  : '50%',
          right                 : 'auto',
          bottom                : 'auto',
          marginRight           : '-50%',
          transform             : 'translate(-50%, -50%)'
        }
      };
    constructor(props){
        super(props);

        this.state = { 
            productDetails : this.props.productDetails,
            key: '',
            categoryId:this.props.categoryId,
            selectValue:" ",
            productCostPrice:0,
            product_discount_json:0,
            product_selling_price_json:0,
            product_cost_price_json:0,
            price:0,
        }
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.getProducts = this.getProducts.bind(this);
        this.makeASale = this.makeASale.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.closeSale = this.closeSale.bind(this);
    }
    getProducts(){
        this.setState({
            productDetails: this.props.productDetails,
            key: this.props.key
        });
    }

    makeASale(e){
        e.preventDefault();
        let ConsumerSecret ="";
        let ConsumerKey ="";
        let quantity =this.quantity.value;
        let discount = this.product_discount_json.value;
        let selectValue= this.payment_option.value;
        let product_selling_price_json = this.product_selling_price_json.value;
        let product_cost_price_json = this.product_cost_price_json.value;

        //Quantity input
        //Discount input
        //Payment Method
        this.setState({
          quantity: this.quantity.value,
          discount: this.product_discount_json.value,
          selectValue: this.payment_option.value,
          product_selling_price_json: this.product_selling_price_json.value,
          product_cost_price_json: this.product_cost_price_json.value,
        });

        //Payment amount.
        let amount = (quantity) * (product_selling_price_json);
        let total_amount = amount - (amount * (discount/100));
        //Make a sale and get feedback
        let token = localStorage.getItem('userToken');
        ConsumerKey = JSON.parse(token).consumerKey;
        ConsumerSecret = JSON.parse(token).consumerSecret;
        
       let payments = "";
        switch(selectValue){
          case "cash":
            payments = `{"name":"CASH","amount":${total_amount},"ref":"none"},{"name":"VISA","amount":0,"ref":"none"},{"name":"CREDIT","amount":0,"ref":"none"},{"name":"CHEQUE","amount":0,"ref":"none"},{"name":"LOYALTY","amount":0,"ref":"none"}`;
          break;
          
          case "mcash":
          payments = `{"name":"CASH","amount":0,"ref":"none"},{"name":"VISA","amount":0,"ref":"none"},{"name":"CREDIT","amount":${total_amount},"ref":"none"},{"name":"CHEQUE","amount":0,"ref":"none"},{"name":"LOYALTY","amount":0,"ref":"none"}`;
          
          break;
          case "card":
            payments = `{"name":"CASH","amount":0,"ref":"none"},{"name":"VISA","amount":${total_amount},"ref":"none"},{"name":"CREDIT","amount":0,"ref":"none"},{"name":"CHEQUE","amount":0,"ref":"none"},{"name":"LOYALTY","amount":0,"ref":"none"}`;
          
          break;
          default:
            payments = `{"name":"CASH","amount":${total_amount},"ref":"none"},{"name":"VISA","amount":0,"ref":"none"},{"name":"CREDIT","amount":0,"ref":"none"},{"name":"CHEQUE","amount":0,"ref":"none"},{"name":"LOYALTY","amount":0,"ref":"none"}`;
          
        }
        let config = {
          "ConsumerKey":ConsumerKey,
          "ConsumerSecret":ConsumerSecret,
          "storeid":224,
          "loginid":1,
          "branch_id":1,
          "sale_lines":[{"cost_price": product_cost_price_json,
                        "selling_price": product_selling_price_json,
                        "tax_amount": this.state.productDetails.product_tax_json[1] ,
                        "movement_amount": 120,
                        "quantity":quantity,
                        "discount_amount":discount,
                        "sku":this.state.productDetails.sku,
                        "product_id":this.state.productDetails.id,
                        "productname":this.state.productDetails.name}],
          "payments":[payments],
          "mover_id":"null"
          }
         this.closeSale(config);
          
      }

      async closeSale(config) {

        MakeASaleData(config).then((results) => {
          MakeASaleData().then((results) => {
            
            if(results.status===1){
              alert("The Purchase has been made successfully. Thank you!");
            }
          });
        });
      }
      



    handleChange(e){
      e.preventDefault();
      this.setState({selectValue:this.payment_option.value});
    }

    openModal() {
        this.setState({modalIsOpen: true});
      }
    
      afterOpenModal() {
        // references are now sync'd and can be accessed.
        this.subtitle.style.color = '#f00';
      }
    
      closeModal() {
        this.setState({modalIsOpen: false});
      }

  render() {
      const product = this.state.productDetails;
      const categoryId = this.state.categoryId;
      let productCostPrice = JSON.parse(product.product_cost_price_json)[categoryId];
      let product_selling_price_json = JSON.parse(product.product_selling_price_json)[categoryId];
      let product_cost_price_json = JSON.parse(product.product_cost_price_json)[categoryId];
      let  product_discount_json = ((product.product_discount_json==="null") ? 0 :  product.product_discount_json);
       
      
    return (
        <div>
        <div className=" col-md-4">
            <div className="paper" id={product.id}>
                <div className="content content__narrow">
                    <div className="article intercom-force-break">
                        <div className="article__meta" dir="ltr">
                            <h3>{product.name}</h3>
                            <img src = {product.photo} alt={product.name}/>
                            <ul>
                                <li><b>name :</b> {product.name}</li>
                                <li><b>variant :</b> {product.variant}</li>
                                <li><b>quantity :</b> {product.quantity}</li>
                                <li><b>sku :</b> {product.sku}</li>
                                <li><b>cost price :</b> {productCostPrice}</li>
                                <li><b>selling price :</b> {product_selling_price_json}</li>
                            </ul>
                            <br />
                            <input  onClick={this.openModal} className="btn btn-primary btn-block br3 btn-lg" type="button" value="Add to Cart" />
        
                        </div>
                    </div>
                </div>
            </div>
            <br />
    </div>
    <div>
        <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={this.customStyles}
            contentLabel="Example Modal"
        >

        <h2 ref={subtitle => this.subtitle = subtitle}>Hello</h2>
        <button onClick={this.closeModal}>close</button>
        
        <div id="fh5co-contact" className="">
        <div className="modal-container m-t-30 m-b-30">
          <div className="row">
            <div className="col-md-12 text-center fh5co-heading">
              <h3 className="fcorange"><b>{product.name}</b></h3>
            </div>
          </div>
          <div className="row">
            <div className="col-md-8 col-md-offset-2">
              <div className="loginForm">
                <form className="contact-form text-caps" onSubmit = {this.makeASale}>
                  <div className="form-group">
                    <label htmlFor="Quantity" className="control-label">Quantity*</label>
                    <input  
                      ref = {quantity => this.quantity = quantity} 
                      className="form-control" 
                      name="quantity" 
                      type="text" 
                      defaultValue={1}
                    />	
                  </div>
                  <div className="form-group">
                    <label htmlFor="Discount" className="control-label">Discount *</label>
                    <input  
                        ref = {discount => this.discount = discount} 
                        className="form-control" 
                        name="discount" 
                        type="text" 
                        disabled = "disabled"
                        defaultValue={product_discount_json}
                        />	


                  </div>
                  <div className="form-group">
                    <label htmlFor="payment_option" className="control-label">Payment Option * </label>
                    <select 
                    ref = {payment_option => this.payment_option = payment_option} 
                      name="payment_option"
                      defaultValue={this.state.selectValue} 
                      onChange={this.handleChange} 
                    >
                        <option name="cash">CASH</option>
                        <option name="mcash">MCASH</option>
                        <option name="card">CARD</option>
                    </select>
                  </div>
                  <div className="form-group">
                  <input 
                    ref = {total_amount => this.total_amount = total_amount} 
                    type="hidden" 
                    value={this.state.total_amount} 
                    name="total_amount"
                  />
                  
                  <input
                  ref = {product_discount_json => this.product_discount_json = product_discount_json} 
                    
                      type="hidden"
                      name="product_discount_json"
                      value={product_discount_json}
                      />
                  
                  <input
                  ref = {product_selling_price_json => this.product_selling_price_json = product_selling_price_json} 
                    
                      type="hidden"
                      name="product_selling_price_json"
                      value={product_selling_price_json}
                      />
                  <input
                  ref = {product_cost_price_json=> this.product_cost_price_json = product_cost_price_json} 
                    
                      type="hidden"
                      name="product_cost_price_json"
                      value={product_cost_price_json}
                      />
                    <input value="Buy" className="btn btn-primary btn-block br3 btn-lg" type="submit" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
        </Modal>
    </div>
</div>
    );
  }
}

export default Product;
