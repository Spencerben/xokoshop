import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import './App.css';
import Routes from './Routes';
import { Grid } from 'react-bootstrap';

import Navigation from './components/navigation/Navigation';
import Footer from './components/footer/Footer';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      title: "Login"
    }
    
    this.onTitleChange = this.onTitleChange.bind(this);
    this.handleTitle = this.handleTitle.bind(this);
    this.checkLogin = this.checkLogin.bind(this);
  }
  onTitleChange(){
    this.handleTitle();
  }
  handleTitle(){
    this.setState({
      title: ""
    })
  }
  checkLogin(){
    let loginToken = (localStorage.getItem('userToken') !== null)?
    localStorage.getItem('userToken'): 
    false;
    if(loginToken === false){
      return <Redirect to="/login" />
     }
  }
  
  render() {
    this.checkLogin();
    return (
      
      <div className="App">
      <Navigation />
        <Grid>
          <br /><br /><br /><br /><br />
          <h1> {this.onTitleChange}</h1>
          <Routes />
          
        </Grid>
        <Footer /> 
      </div>
      
    );
  }
}

export default App;
