import axios from 'axios';

const BASE_URL = 'https://private-anon-793e0ab7fe-bambapospub.apiary-mock.com';

export { MakeASaleData };

function MakeASaleData(data) {
  const url = `${BASE_URL}/office/branches/closesale`;
  //const config = JSON.stringify(data);
  const headers = {
    // 'Authorization': `bearer ${token}`,
    'Content-Type': 'application/json'
  }
  return axios
  .post(url, data,headers)
  .then(response => {
      return response.data
    });
}