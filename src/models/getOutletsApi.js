import axios from 'axios';

const BASE_URL = 'https://private-anon-793e0ab7fe-bambapospub.apiary-mock.com';

export { GetOutletsData, GetCategoriesData};

function GetCategoriesData() {
  const url = `${BASE_URL}/office/categories/get`;
  const config = JSON.parse(localStorage.getItem('userToken'));
  
  return axios
  .post(url, config)
  .then(response => {
      return response.data;
    })
    .then(data => {
        return data.response
    });
}

function GetOutletsData() {
  const url = `${BASE_URL}/office/branches/get`;
  const config = {
    "ConsumerKey": "lXq3f6CfoGkD",
    "ConsumerSecret": "wpiEboCW4H"
  };
  return axios
  .post(url, config)
  .then(response => {
      return response.data;
    })
    .then(data => {
        return data.response
    });
}

