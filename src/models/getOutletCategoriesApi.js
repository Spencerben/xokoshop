import axios from 'axios';

const BASE_URL = 'https://private-anon-793e0ab7fe-bambapospub.apiary-mock.com';

export { GetOutletCategoriesData };
function GetOutletCategoriesData(outletId) {
  const url = `${BASE_URL}/office/products/get`;
  const config = JSON.parse(localStorage.getItem('userToken'));
  
  return axios
  .post(url, config)
  .then(response => {

      //since there isnt an available POST/GET request to filter for categories,
      //we shall get all products and filter for categories. Not
      //best approach
    
      return response.data;
    })
    .then(data => {
        return data.response
    });

}

