import React from 'react';
import {BrowserRouter, Switch, Route } from 'react-router-dom';
import Products from './components/Products/Products';
import Outlets from './components/Outlets/Outlets';
import Categories from './components/Categories/Categories';
import MakeASale from './components/MakeASale/MakeASale';
import Login from './components/login/Login';





const Routes = () => {
    
    return (
        
    <BrowserRouter>
        <Switch>
            <Route exact path = '/' component = {Login} />
            <Route  path = '/login' component = {Login} />
            <Route  path = '/outlets' component = {Outlets} />
            <Route  path = '/categories' component = {Categories} />
            <Route  path = '/category/:id/Products/' component = {Products} />
            <Route  path = '/Products' component = {Products} />
            <Route  path = '/MakeASale' component = {MakeASale} />
        </Switch>
    </BrowserRouter>
    );
}

export default Routes;